<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="../../inc/meta.jsp" />
<jsp:include page="../../inc/esyui.jsp" />
<script type="text/javascript">
	sy.ns("sy.center");
	$(function() {
		sy.center.editRow = 'undefined' ;
		sy.center.dg = $('#dg').datagrid({
			url : sy.bp() + '/InfoOutAction?action=all',
			fitColumns : true,
			nowrap : false,
			idField : 'id',
			fit : true,
			pagination : true,
			pageSize : 2,
			pageList : [ 2, 4, 6, 8 ],
			sortName : 'id',
			sortOrder : 'desc',
			columns : [ [ {
				field : 'id',
				sortable : true,
				title : 'id',
				width : 100
			}, {
				field : 'name',
				title : '学生名称',
				editor:{
					type:'validatebox',
					options:{
						required:true
					}
				}
			},  {
				field : 'number',
				title : '学生学号',
				width : 100,
				editor:{
					type:'validatebox',
					options:{
						required:true
					}
				}
			}, {
				field : 'outdate',
				title : '迁出日期',
				width : 100,
				editor:{
					type:'validatebox',
					options:{
						required:true
					}
				}
			},{
				field : 'news',
				title : '备注',
				width : 100,
				editor:{
					type:'validatebox',
					options:{
						required:true
					}
				}
			} ] ],
			toolbar : [ {
				text : '新增',
				iconCls : 'icon-add',
				handler : function() {
				    console.info(sy.center.editRow);
					/*判断当前行是否处于编辑状态*/
					if(sy.center.editRow != 'undefined') {
						sy.center.dg.datagrid('endEdit',sy.center.editRow);
					}
					/*编辑非编辑状态*/
					if(sy.center.editRow == 'undefined') {
						sy.center.dg.datagrid('insertRow', {
							index : 0, // index start with 0
							row : {
							}
						});
	
						sy.center.dg.datagrid('beginEdit', 0);
						sy.center.editRow = 0 ;
					}
					
				}
			}, '-', {
				text : '修改',
				iconCls : 'icon-edit',
				handler : function() {
					/*判断当前行是否处于编辑状态*/
					if(sy.center.editRow != 'undefined') {
						sy.center.dg.datagrid('endEdit',sy.center.editRow);
					}
					
					if(sy.center.editRow == 'undefined') {
						// 判断当前用户是否有选中的行  getSelections
						var rows = sy.center.dg.datagrid('getSelections') ;
						var len = rows.length;
						// 这代表选择了多行 
						if(len !=0 && len !=1) {
							var row=[] ;// 定义了一个空数组
							for(var i = 0 ;i< len ;i++) {
								row.push(rows[i].name);// 将值插入到数组中
							}
							// var str = ["asb","bdc"] ;
							// asb,bdc .split(",")
							var str = row.join(',');
							$.messager.show({
								title:'提示信息',
								msg:'你选择了【'+str+'】,'+len+'个用户进行操作，应该只选择一个用户',
								timeout:5000,
								showType:'slide'
							});

							
						} else if(len == 1) { // 这代表选择了一行
							//打开当前行的编辑状态
							var index = sy.center.dg.datagrid('getRowIndex',rows[0]) ;
							sy.center.dg.datagrid('beginEdit',index) ;
							sy.center.editRow = index ;
							// 取消当前行的选中状态
							sy.center.dg.datagrid('unselectAll');
						} else { // 代表一行都没有选
							$.messager.alert('提示信息','请选择一行'); 
						}
					}
				}
			}, '-', {
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
					// 判断当前用户是否有选中的行  getSelections
					var rows = sy.center.dg.datagrid('getSelections') ;
					var len = rows.length;
					if(len >= 1) {
						var ids=[] ;// 定义了一个空数组
						for(var i = 0 ;i< len ;i++) {
							ids.push(rows[i].id);// 将值插入到数组中
						}
						//[1,2,3,4]---->1,2,3,4
						var strIds = ids.join(',');
						// 向后台发送一个ajax请求
						$.ajax({
						   type: "POST",
						   url: sy.bp()+"/InfoOutAction?action=delete",
						   data: "ids="+strIds,
						   dataType:"json",
						   success: function(data){
						     if(data && data.flag) {
						     	// 删除成功 load
						     	sy.center.dg.datagrid('load');
						     } 
						     $.messager.alert('提示信息',data.msg);
						    
						   }
							
						}) ;
					} else {
						$.messager.alert('提示信息','至少选择一行'); 
					}
				}
			}, '-', {
				text : '保存',
				iconCls : 'icon-save',
				handler : function() {
					/*判断当前行是否处于编辑状态*/
					if(sy.center.editRow != 'undefined') {
						sy.center.dg.datagrid('endEdit',sy.center.editRow);
						sy.center.dg.datagrid('unselectAll');
					}
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-redo',
				handler : function() {
					sy.center.dg.datagrid('unselectAll');
				}
			}, '-', {
				text : '取消编辑',
				iconCls : 'icon-redo',
				handler : function() {
					if(sy.center.editRow != 'undefined') {
						sy.center.dg.datagrid('endEdit',sy.center.editRow);
						sy.center.dg.datagrid('unselectAll');
						sy.center.dg.datagrid('rejectChanges');
					}
				}
			} ],
			onAfterEdit:function(rowIndex, rowData, changes) {
				//console.info(rowData);
				var inserted = sy.center.dg.datagrid('getChanges','inserted');
				var updated = sy.center.dg.datagrid('getChanges','updated');
				if(inserted.length<1 && updated.length <1) {
					sy.center.editRow = 'undefined'
					return  ;
				}
				var href  ;
				if(inserted.length > 0) {
					href = sy.bp()+"/InfoOutAction?action=add"; 
				}
				if(updated.length > 0) {
					href = sy.bp()+"/InfoOutAction?action=edit"; 
				}
				$.ajax({
				   type: "POST",
				   url: href,
				   data: rowData,
				   dataType:"json",
				   success: function(data){
				      if(data && data.flag) {
				      	sy.center.dg.datagrid('reload');
				      	sy.center.dg.datagrid('acceptChanges');
				      	$.messager.show({
								title:'提示信息',
								msg:data.msg,
								timeout:2000,
								showType:'slide'
						});
				      	
				      }else{
				      	 $.messager.alert('提示信息',data.msg);
				      	 sy.center.dg.datagrid('rejectChanges');
				      }
				       sy.center.dg.datagrid('unselectAll');
				       sy.center.editRow = 'undefined'
				   }
				});
				
			}

		});

	});
	function qq(value,name){ 
	  
        href = sy.bp()+"/InfoOutAction?action=sel"; 
    } 
</script>
</head>

<body>
	<p>请输入名称进行模糊查询<input id="ss" class="easyui-searchbox" style="width:300px"  
        data-options="searcher:qq,prompt:'Please Input Value',menu:'#mm'"></input> </p> 
	<table id="dg"></table>
</body>

</html>
