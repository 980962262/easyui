<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<jsp:include page="../../inc/meta.jsp" />
	<jsp:include page="../../inc/esyui.jsp" />
  </head>
  
 <body>  
    <div id="tt" class="easyui-tabs" style="width:500px;height:250px;">  
	    <div title="Tab1" style="padding:20px;">  
	        tab1   
	    </div>  
	    <div title="Tab2" data-options="closable:true" style="overflow:auto;padding:20px;display:none;">  
	        tab2   
	    </div>  
	    <div title="Tab3" data-options="iconCls:'icon-reload',closable:true" style="padding:20px;display:none;">  
	        tab3   
	    </div>  
	</div>  
</body>  

</html>
