<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<jsp:include page="../../inc/meta.jsp" />
	<jsp:include page="../../inc/esyui.jsp" />
	<script type="text/javascript">
		$(function(){
			$('#centerTabs').tabs({
				border:false,
				title:'首页',
				fit:true,
				closeable:true
			});
			
			setTimeout(function(){
			    var href=sy.bp()+"/HomeAction?action=home" ;
				$('#centerTabs').tabs('add',{   
				    title:'New Tab',   
				    content:'<iframe src="'+href+'" frameborder="0px" scrolling="no" style="width:100%;height:99.8%;border:0px;"></iframe>',   
				    closable:true,
				    fit:true  
				}); 
			}, 0);
			
			$('#btn').click(function(){
				addTabFun({src:'index.jsp',title:'New Tab'});
			});
			
		});
		
		function addTabFun(ops) {
			var href= ops.src ;
				if($('#centerTabs').tabs('exists',ops.title)) {
					$('#centerTabs').tabs('close',ops.title)
				}
				$('#centerTabs').tabs('add',{   
				    title:ops.title,   
				    content:'<iframe src="'+href+'" frameborder="0px" scrolling="no" style="width:100%;height:99.8%;border:0px;"></iframe>',   
				    closable:true,
				    fit:true  
				}); 
		}
	</script>
  </head>
  
 <body>
    <a id="btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">新增选项卡</a>   
    <div id="centerTabs"></div>
</body>  

</html>
