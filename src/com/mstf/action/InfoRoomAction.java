package com.mstf.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mstf.bean.Message;
import com.mstf.service.InfoRoomService;
import com.mstf.service.impl.InfoRoomServiceImpl;
import com.mstf.util.CovertObjectToJSON;

public class InfoRoomAction extends HttpServlet {

	private InfoRoomService infoRoomService = new InfoRoomServiceImpl();
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response) ;
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8") ;
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action") ;
		if("all".equals(action)) {
			String order = request.getParameter("order") ;	// 排序方式i
			int page = Integer.parseInt(request.getParameter("page")) ;	// 当前页
			int rows = Integer.parseInt(request.getParameter("rows")) ;	// 每页多少条数据
			String sort = request.getParameter("sort") ;	// 排序的列
			Map<String,Object> maps = infoRoomService.findAll(page,rows,sort,order)  ;
			out.print(CovertObjectToJSON.covertToJson(maps)) ;
		}
		
		if("delete".equals(action)) {
			String strId = request.getParameter("ids") ;
			String[] strIds =strId.split(",");
			Message msg = new Message() ;
			try {
				for(String id :strIds) {
					System.out.println(id);
					infoRoomService.delete(Integer.parseInt(id));
				}
				msg.setFlag(true) ;
				msg.setMsg("删除成功") ;
			} catch (NumberFormatException e) {
				msg.setFlag(false) ;
				msg.setMsg("删除失败") ;
				e.printStackTrace();
			}
			out.print(CovertObjectToJSON.covertToJson(msg)) ;
		}
		if("add".equals(action)) {
			
			String name = request.getParameter("name") ;
			String type = request.getParameter("type") ;
			String news = request.getParameter("news") ;
			System.out.println(name+":"+type+":"+news);
			int result=infoRoomService.into(name,type,news);
			if(result!=0){
				System.out.println(true);
			}else{
				System.out.println(false);
			}
		}
		if("edit".equals(action)) {
			int id= Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name") ;
			String type = request.getParameter("type") ;
			String news = request.getParameter("news") ;
			System.out.println(name+":"+type+":"+news+":"+id);
			int result=infoRoomService.edit(name,type,news,id);
			if(result!=0){
				System.out.println(true);
			}else{
				System.out.println(false);
			}
		}
		if("sel".equals(action)) {
			String name=request.getParameter("value");
			System.out.println(name);
			String order = request.getParameter("order") ;	// 排序方式i
			int page = Integer.parseInt(request.getParameter("page")) ;	// 当前页
			int rows = Integer.parseInt(request.getParameter("rows")) ;	// 每页多少条数据
			String sort = request.getParameter("sort") ;	// 排序的列
			Map<String,Object> maps = infoRoomService.sel(page,rows,sort,order,name)  ;
			out.print(CovertObjectToJSON.covertToJson(maps)) ;
		}
		out.flush();
		out.close();
	}

}
