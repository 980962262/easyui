package com.mstf.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mstf.bean.Portal;
import com.mstf.service.PortalService;
import com.mstf.service.impl.PortalServiceImpl;
import com.mstf.util.CovertObjectToJSON;

public class PortalAction extends HttpServlet {
	private PortalService portalService = new PortalServiceImpl() ;
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response) ;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		List<Portal> portals = portalService.getAll() ;
		out.print(CovertObjectToJSON.covertToJson(portals)) ;
		out.flush();
		out.close();
	}

}
