package com.mstf.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeAction extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		String action = request.getParameter("action");
		// 1.北部面板
		if ("north".equals(action)) {

			request.getRequestDispatcher("/layout/north.jsp").forward(request,
					response);
		}
		// 2.南部面板
//		if ("south".equals(action)) {
//
//			request.getRequestDispatcher("/layout/south.jsp").forward(request,
//					response);
//		}
		// 3.西部面板
		if ("west".equals(action)) {

			request.getRequestDispatcher("/layout/west.jsp").forward(request,
					response);
		}
		// 4.东部面板
//		if ("east".equals(action)) {
//
//			request.getRequestDispatcher("/layout/east.jsp").forward(request,
//					response);
//		}
		// 5.中部面板
		if ("center".equals(action)) {

			request.getRequestDispatcher("/layout/center.jsp").forward(request,
					response);
		}
		// 6.首页
		if ("home".equals(action)) {
			
			request.getRequestDispatcher("/layout/center.jsp").forward(request,
					response);
		}
		
		
		// 5.中部面板
			if ("weststu".equals(action)) {

				request.getRequestDispatcher("/layout/weststu.jsp").forward(request,
						response);
			}
				
				
			// 5.中部面板
			if ("homestu".equals(action)) {
				
				request.getRequestDispatcher("/layout/homestu.jsp").forward(request,
						response);
			}
	}

}
