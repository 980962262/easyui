package com.mstf.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mstf.bean.Message;
import com.mstf.bean.Sudents;
import com.mstf.bean.User;
import com.mstf.service.UserService;
import com.mstf.service.impl.UserServiceImpl;
import com.mstf.util.CovertObjectToJSON;

public class UserAction extends HttpServlet {
    private UserService userService = new UserServiceImpl();

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        Message msg = new Message();
        if ("login".equals(action)) {
            String uname = request.getParameter("uname");
            String upass = request.getParameter("upass");
            String usf = request.getParameter("sf");
            System.out.println(usf);
            User user = null;
            Sudents stu = null;
            if ("管理员".equals(usf)) {
                System.out.println("这是管理员");
                user = userService.getUserByNameAndPwd(uname, upass);
                if (user != null) {
                    // 开始响应信息{"obj":user,"msg":"登录成功","flag":true}
                    msg.setFlag(true);
                    msg.setMsg("登录成功");
                    msg.setObj(user);
                    msg.setType(0);


                } else {
                    // 开始响应信息
                    msg.setFlag(false);
                    msg.setMsg("登录失败");
                }
            } else {
                System.out.println("这是学生");
                stu = userService.getUserStuByNameAndPwd(uname, upass);
                if (stu != null) {
                    // 开始响应信息{"obj":user,"msg":"登录成功","flag":true}
                    msg.setFlag(true);
                    msg.setMsg("登录成功");
                    msg.setObj(stu);
                    msg.setType(1);
//					response.sendRedirect("jsp/homeAction/homestu.jsp");

                } else {
                    // 开始响应信息
                    msg.setFlag(false);
                    msg.setMsg("登录失败");
                }
            }

            out.print(CovertObjectToJSON.covertToJson(msg));
        }
        // 注册
        if ("reg".equals(action)) {
            String name = request.getParameter("name");
            String pass = request.getParameter("pass");

            int result = userService.save(name, pass);
            if (result > 0) {
                msg.setFlag(true);
                msg.setMsg("保存成功");
            } else {
                msg.setFlag(false);
                msg.setMsg("保存失败");
            }
            out.print(CovertObjectToJSON.covertToJson(msg));
        }
        // 注册
        if ("logout".equals(action)) {
            msg.setFlag(true);
            msg.setMsg("注销成功");
            out.print(CovertObjectToJSON.covertToJson(msg));
        }
        out.flush();
        out.close();
    }

}
