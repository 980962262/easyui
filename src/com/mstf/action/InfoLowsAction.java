package com.mstf.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mstf.bean.Message;
import com.mstf.service.InfoLowsService;
import com.mstf.service.impl.InfoLowsServiceImpl;
import com.mstf.util.CovertObjectToJSON;

public class InfoLowsAction extends HttpServlet {

	private InfoLowsService infoService = new InfoLowsServiceImpl();
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response) ;
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8") ;
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action") ;
		if("all".equals(action)) {
			String order = request.getParameter("order") ;	// 排序方式i
			int page = Integer.parseInt(request.getParameter("page")) ;	// 当前页
			int rows = Integer.parseInt(request.getParameter("rows")) ;	// 每页多少条数据
			String sort = request.getParameter("sort") ;	// 排序的列
			Map<String,Object> maps = infoService.findAll(page,rows,sort,order)  ;
			out.print(CovertObjectToJSON.covertToJson(maps)) ;
		}
		
		if("delete".equals(action)) {
			String strId = request.getParameter("ids") ;
			String[] strIds =strId.split(",");
			Message msg = new Message() ;
			try {
				for(String id :strIds) {
					infoService.delete(Integer.parseInt(id));
				}
				msg.setFlag(true) ;
				msg.setMsg("删除成功") ;
			} catch (NumberFormatException e) {
				msg.setFlag(false) ;
				msg.setMsg("删除失败") ;
				e.printStackTrace();
			}
			out.print(CovertObjectToJSON.covertToJson(msg)) ;
		}
		if("add".equals(action)) {
			
			String name = request.getParameter("name") ;
			String number = request.getParameter("number") ;
			String address = request.getParameter("date") ;
			String birthday = request.getParameter("news") ;
			System.out.println(name+":"+number+":"+address+":"+birthday);
			int result=infoService.into(name,number,address,birthday);
			if(result!=0){
				System.out.println(true);
			}else{
				System.out.println(false);
			}
		}
		if("edit".equals(action)) {
			int id= Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name") ;
			String number = request.getParameter("number") ;
			String address = request.getParameter("date") ;
			String birthday = request.getParameter("news") ;
			System.out.println(name+":"+number+":"+address+":"+birthday+":"+id);
			int result=infoService.edit(name,number,address,birthday,id);
			if(result!=0){
				System.out.println(true);
			}else{
				System.out.println(false);
			}
		}
		if("sel".equals(action)) {
			String name=request.getParameter("value");
			System.out.println(name);
			String order = request.getParameter("order") ;	// 排序方式i
			int page = Integer.parseInt(request.getParameter("page")) ;	// 当前页
			int rows = Integer.parseInt(request.getParameter("rows")) ;	// 每页多少条数据
			String sort = request.getParameter("sort") ;	// 排序的列
			Map<String,Object> maps = infoService.sel(page,rows,sort,order,name)  ;
			out.print(CovertObjectToJSON.covertToJson(maps)) ;
		}
		out.flush();
		out.close();
	}

}
