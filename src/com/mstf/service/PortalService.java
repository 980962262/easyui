package com.mstf.service;

import java.util.List;

import com.mstf.bean.Portal;

public interface PortalService {

	List<Portal> getAll();

}
