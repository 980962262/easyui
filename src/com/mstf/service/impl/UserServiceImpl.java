package com.mstf.service.impl;

import java.sql.SQLException;

import com.mstf.bean.Sudents;
import com.mstf.bean.User;
import com.mstf.service.UserService;
import com.mstf.util.DBUtil;

public class UserServiceImpl extends DBUtil implements UserService {

	@Override
	public User getUserByNameAndPwd(String uname, String upass) {
		User user = null ;
		String sql ="select * from mstf_user where user_name=? and user_pass=?" ;
		rs = commonQuery(sql, uname,upass) ;
		try {
			if(rs.next()) {
				user = new User(rs.getInt("id"),rs.getString("user_name"),rs.getString("user_pass")) ;
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return user;
	}

	@Override
	public int save(String name, String pass) {
		String sql = "insert into mstf_user(user_name,user_pass) values(?,?)" ;
		return commonUpdate(sql, name,pass) ;
	}

	@Override
	public Sudents getUserStuByNameAndPwd(String uname, String upass) {
		Sudents stu = null ;
		String sql ="select * from student where name=? and number=?" ;
		rs = commonQuery(sql, uname,upass) ;
		try {
			if(rs.next()) {
				stu = new Sudents(rs.getInt("id"),rs.getString("name"),rs.getString("number")) ;
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return stu;
	}

}
