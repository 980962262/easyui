package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mstf.bean.Info;
import com.mstf.bean.InfoLows;
import com.mstf.service.InfoLowsService;
import com.mstf.service.InfoService;
import com.mstf.util.CovertObjectToJSON;
import com.mstf.util.DBUtil;

public class InfoLowsServiceImpl extends DBUtil implements InfoLowsService {
	//缺寝信息表
	@Override
	public Map<String, Object> findAll(int page, int rows, String sort,
			String order) {
		Map<String,Object> maps = new HashMap<String, Object>();
		List<InfoLows> infos = getAll(page,rows,sort,order);
		int rowCount = getRowCount() ;
		maps.put("total", rowCount) ;
		maps.put("rows", infos) ;
		return maps;
	}

	private int getRowCount() {
		int rowCount = 0 ;
		rs = commonQuery("select count(*) from lows", null) ;
		try {
			if(rs.next()) {
				rowCount = rs.getInt(1) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return rowCount;
	}

	private List<InfoLows> getAll(int page, int rows, String sort,
			String order) {
		List<InfoLows> infos = new ArrayList<InfoLows>() ;
		String sql = "select * from lows where 1=1 " ;
		if(sort != null && order != null) {
			sql += " order by "+sort +" " + order ; 
		}
		sql += " limit ?,?" ;
		rs = commonQuery(sql, rows*(page-1),rows) ;
		try {
			while(rs.next()) {
				InfoLows info = new InfoLows() ;
				info.setId(rs.getInt("id")) ;
				info.setName(rs.getString("name")) ;
				info.setNumber(rs.getString("number")) ;
				info.setDate(rs.getString("date")) ;
				info.setNews(rs.getString("news")) ;
				infos.add(info) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return infos;
	}

	@Override
	public void delete(int id) {
		
		commonUpdate("delete from lows where id =?", id) ;
		
	}

	@Override
	public int into(String name,String number, String address, String birthday) {
			int result=0;
			String sql ="INSERT INTO lows(name,number,date,news) values(?,?,?,?)" ;
			result=commonUpdate(sql, name,number,address,birthday) ;
			return result;
		}

		
		@Override
		public int edit( String name, String number, String address,String birthday,int id) {
			
			String sql ="UPDATE lows SET name=?,number=?,date=?,news=? WHERE id=?" ;
			int result=commonUpdate(sql, name,number,address,birthday,id) ;
			return result;
		}

		@Override
		public Map<String, Object> sel(int page, int rows, String sort,
				String order, String name) {
			Map<String,Object> maps = new HashMap<String, Object>();
			List<InfoLows> infos = select(page,rows,sort,order,name);
			int rowCount = getRowCount() ;
			maps.put("total", rowCount) ;
			maps.put("rows", infos) ;
			return maps;
		}
		private List<InfoLows> select(int page, int rows, String sort,
				String order,String name) {
			List<InfoLows> infos = new ArrayList<InfoLows>() ;
			String sql = "select * from lows where name=? and 1=1 " ;
			if(sort != null && order != null) {
				sql += " order by "+sort +" " + order ; 
			}
			sql += " limit ?,?" ;
			rs = commonQuery(sql, rows*(page-1),rows) ;
			try {
				while(rs.next()) {
					InfoLows info = new InfoLows() ;
					info.setId(rs.getInt("id")) ;
					info.setName(rs.getString("name")) ;
					info.setNumber(rs.getString("number")) ;
					info.setDate(rs.getString("date")) ;
					info.setNews(rs.getString("news")) ;
					infos.add(info) ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				closeAll(rs, pstmt, conn) ;
			}
			return infos;
		}
		
}
