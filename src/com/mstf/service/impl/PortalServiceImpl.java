package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mstf.bean.Portal;
import com.mstf.service.PortalService;
import com.mstf.util.DBUtil;

public class PortalServiceImpl extends DBUtil implements PortalService {

	@Override
	public List<Portal> getAll() {
		List<Portal> portals = new ArrayList<Portal>() ;
		rs = commonQuery("select * from portal", null) ;
		try {
			while(rs.next()) {
				
				portals.add(new Portal(rs.getInt("id"), rs.getString("title"), rs.getString("path"))) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return portals;
	}
	
	
}
