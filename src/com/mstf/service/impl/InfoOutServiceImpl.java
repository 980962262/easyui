package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mstf.bean.Info;
import com.mstf.bean.InfoOut;
import com.mstf.service.InfoOutService;
import com.mstf.util.CovertObjectToJSON;
import com.mstf.util.DBUtil;

public class InfoOutServiceImpl extends DBUtil implements InfoOutService {
	//迁出信息表
	@Override
	public Map<String, Object> findAll(int page, int rows, String sort,
			String order) {
		Map<String,Object> maps = new HashMap<String, Object>();
		List<InfoOut> infos = getAll(page,rows,sort,order);
		int rowCount = getRowCount() ;
		maps.put("total", rowCount) ;
		maps.put("rows", infos) ;
		return maps;
	}

	private int getRowCount() {
		int rowCount = 0 ;
		rs = commonQuery("select count(*) from outs", null) ;
		try {
			if(rs.next()) {
				rowCount = rs.getInt(1) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return rowCount;
	}

	private List<InfoOut> getAll(int page, int rows, String sort,
			String order) {
		List<InfoOut> infos = new ArrayList<InfoOut>() ;
		String sql = "select * from outs where 1=1 " ;
		if(sort != null && order != null) {
			sql += " order by "+sort +" " + order ; 
		}
		sql += " limit ?,?" ;
		rs = commonQuery(sql, rows*(page-1),rows) ;
		try {
			while(rs.next()) {
				InfoOut info = new InfoOut() ;
				info.setId(rs.getInt("id")) ;
				info.setName(rs.getString("Name")) ;
				info.setNumber(rs.getString("number")) ;
				info.setOutDate(rs.getString("outdate")) ;
				info.setNews(rs.getString("news")) ;
				infos.add(info) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return infos;
	}

	@Override
	public void delete(int id) {
		
		commonUpdate("delete from outs where id =?", id) ;
		
	}

	@Override
	public int into(String name, String number,String date, String news) {
			int result=0;
			String sql ="INSERT INTO outs(name,number,outdate,news) values(?,?,?,?)" ;
			result=commonUpdate(sql, name,number,date,news) ;
			return result;
		}

		
		@Override
		public int edit( String name, String number,String date, String news,int id) {
			
			String sql ="UPDATE outs SET name=?,number=?,outdate=?,news=? WHERE id=?" ;
			int result=commonUpdate(sql, name,number,date,news,id) ;
			return result;
		}

		@Override
		public Map<String, Object> sel(int page, int rows, String sort,
				String order, String name) {
			Map<String,Object> maps = new HashMap<String, Object>();
			List<InfoOut> infos = select(page,rows,sort,order,name);
			int rowCount = getRowCount() ;
			maps.put("total", rowCount) ;
			maps.put("rows", infos) ;
			return maps;
		}
		private List<InfoOut> select(int page, int rows, String sort,
				String order,String name) {
			List<InfoOut> infos = new ArrayList<InfoOut>() ;
			String sql = "select * from outs where roomName=? and 1=1 " ;
			if(sort != null && order != null) {
				sql += " order by "+sort +" " + order ; 
			}
			sql += " limit ?,?" ;
			rs = commonQuery(sql, rows*(page-1),rows) ;
			try {
				while(rs.next()) {
					InfoOut info = new InfoOut() ;
					info.setId(rs.getInt("id")) ;
					info.setName(rs.getString("Name")) ;
					info.setNumber(rs.getString("number")) ;
					info.setOutDate(rs.getString("outdate")) ;
					info.setNews(rs.getString("news")) ;
					infos.add(info) ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				closeAll(rs, pstmt, conn) ;
			}
			return infos;
		}
		
}
