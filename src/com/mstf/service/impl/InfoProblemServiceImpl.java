package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mstf.bean.InfoLows;
import com.mstf.bean.InfoProblem;
import com.mstf.bean.InfoRequest;
import com.mstf.service.InfoLostService;
import com.mstf.service.InfoProblemService;
import com.mstf.service.InfoRequestService;
import com.mstf.util.CovertObjectToJSON;
import com.mstf.util.DBUtil;

public class InfoProblemServiceImpl extends DBUtil implements InfoProblemService {
	//销寝申请表
	@Override
	public Map<String, Object> findAll(int page, int rows, String sort,
			String order) {
		Map<String,Object> maps = new HashMap<String, Object>();
		List<InfoProblem> infos = getAll(page,rows,sort,order);
		int rowCount = getRowCount() ;
		maps.put("total", rowCount) ;
		maps.put("rows", infos) ;
		return maps;
	}

	private int getRowCount() {
		int rowCount = 0 ;
		rs = commonQuery("select count(*) from problem", null) ;
		try {
			if(rs.next()) {
				rowCount = rs.getInt(1) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return rowCount;
	}

	private List<InfoProblem> getAll(int page, int rows, String sort,
			String order) {
		List<InfoProblem> infos = new ArrayList<InfoProblem>() ;
		String sql = "select * from problem where 1=1 " ;
		if(sort != null && order != null) {
			sql += " order by "+sort +" " + order ; 
		}
		sql += " limit ?,?" ;
		rs = commonQuery(sql, rows*(page-1),rows) ;
		try {
			while(rs.next()) {
				InfoProblem info = new InfoProblem() ;
				info.setId(rs.getInt("id")) ;
				info.setDate(rs.getString("date")) ;
				info.setProblems(rs.getString("problems")) ;
				infos.add(info) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return infos;
	}

	@Override
	public void delete(int id) {
		
		commonUpdate("delete from problem where id =?", id) ;
		
	}

	@Override
	public int into(String date, String problem) {
			int result=0;
			String sql ="INSERT INTO problem(date,problems) values(?,?)" ;
			result=commonUpdate(sql, date,problem) ;
			return result;
		}

		
		@Override
		public int edit(String date, String problem,int id) {
			
			String sql ="UPDATE problem SET date=?,problems=? WHERE id=?" ;
			int result=commonUpdate(sql, date,problem,id) ;
			return result;
		}

		@Override
		public Map<String, Object> sel(int page, int rows, String sort,
				String order, String name) {
			Map<String,Object> maps = new HashMap<String, Object>();
			List<InfoProblem> infos = select(page,rows,sort,order,name);
			int rowCount = getRowCount() ;
			maps.put("total", rowCount) ;
			maps.put("rows", infos) ;
			return maps;
		}
		private List<InfoProblem> select(int page, int rows, String sort,
				String order,String name) {
			List<InfoProblem> infos = new ArrayList<InfoProblem>() ;
			String sql = "select * from problem where name=? and 1=1 " ;
			if(sort != null && order != null) {
				sql += " order by "+sort +" " + order ; 
			}
			sql += " limit ?,?" ;
			rs = commonQuery(sql, rows*(page-1),rows) ;
			try {
				while(rs.next()) {
					InfoProblem info = new InfoProblem() ;
					info.setId(rs.getInt("id")) ;
					info.setDate(rs.getString("date")) ;
					info.setProblems(rs.getString("problems")) ;
					infos.add(info) ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				closeAll(rs, pstmt, conn) ;
			}
			return infos;
		}
		
}
