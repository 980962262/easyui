package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mstf.bean.Info;
import com.mstf.service.InfoService;
import com.mstf.util.CovertObjectToJSON;
import com.mstf.util.DBUtil;

public class InfoServiceImpl extends DBUtil implements InfoService {
    //学生信息表
    @Override
    public Map<String, Object> findAll(int page, int rows, String sort,
                                       String order) {
        Map<String, Object> maps = new HashMap<String, Object>();
        List<Info> infos = getAll(page, rows, sort, order);
        int rowCount = getRowCount();
        maps.put("total", rowCount);
        maps.put("rows", infos);
        return maps;
    }

    private int getRowCount() {
        int rowCount = 0;
        rs = commonQuery("select count(*) from Student", null);
        try {
            if (rs.next()) {
                rowCount = rs.getInt(1);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            closeAll(rs, pstmt, conn);
        }
        return rowCount;
    }

    private List<Info> getAll(int page, int rows, String sort,
                              String order) {
        List<Info> infos = new ArrayList<Info>();
        String sql = "select * from Student where 1=1 ";
        if (sort != null && order != null) {
            sql += " order by " + sort + " " + order;
        }
        sql += " limit ?,?";
        rs = commonQuery(sql, rows * (page - 1), rows);
        try {
            while (rs.next()) {
                Info info = new Info();
                info.setId(rs.getInt("id"));
                info.setName(rs.getString("name"));
                info.setNumber(rs.getString("number"));
                info.setAge(rs.getInt("age"));
                info.setStats(rs.getString("stats"));
                info.setRoom(rs.getString("room"));
                infos.add(info);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            closeAll(rs, pstmt, conn);
        }
        return infos;
    }

    @Override
    public void delete(int id) {

        commonUpdate("delete from Student where id =?", id);

    }

    @Override
    public int into(String name, String number, int age, String stats, String birthday) {
        int result = 0;
        String sql = "INSERT INTO Student(name,number,age,stats,room) values(?,?,?,?,?)";
        result = commonUpdate(sql, name, number, age, stats, birthday);
        return result;
    }


    @Override
    public int edit(String name, String number, int age, String stats, String birthday, int id) {

        String sql = "UPDATE Student SET name=?,number=?,age=?,stats=?,room=? WHERE id=?";
        int result = commonUpdate(sql, name, number, age, stats, birthday, id);
        return result;
    }

    @Override
    public Map<String, Object> sel(int page, int rows, String sort,
                                   String order, String name) {
        Map<String, Object> maps = new HashMap<String, Object>();
        List<Info> infos = select(page, rows, sort, order, name);
        int rowCount = getRowCount();
        maps.put("total", rowCount);
        maps.put("rows", infos);
        return maps;
    }

    private List<Info> select(int page, int rows, String sort,
                              String order, String name) {
        List<Info> infos = new ArrayList<Info>();
        String sql = "select * from Student where name=? and 1=1 ";
        if (sort != null && order != null) {
            sql += " order by " + sort + " " + order;
        }
        sql += " limit ?,?";
        rs = commonQuery(sql, rows * (page - 1), rows);
        try {
            while (rs.next()) {
                Info info = new Info();
                info.setId(rs.getInt("id"));
                info.setName(rs.getString("name"));
                info.setNumber(rs.getString("number"));
                info.setAge(rs.getInt("age"));
                info.setStats(rs.getString("stats"));
                info.setRoom(rs.getString("room"));
                infos.add(info);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            closeAll(rs, pstmt, conn);
        }
        return infos;
    }

}
