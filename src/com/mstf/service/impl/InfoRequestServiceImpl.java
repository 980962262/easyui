package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mstf.bean.InfoLows;
import com.mstf.bean.InfoRequest;
import com.mstf.service.InfoLostService;
import com.mstf.service.InfoRequestService;
import com.mstf.util.CovertObjectToJSON;
import com.mstf.util.DBUtil;

public class InfoRequestServiceImpl extends DBUtil implements InfoRequestService {
	//销寝申请表
	@Override
	public Map<String, Object> findAll(int page, int rows, String sort,
			String order) {
		Map<String,Object> maps = new HashMap<String, Object>();
		List<InfoRequest> infos = getAll(page,rows,sort,order);
		int rowCount = getRowCount() ;
		maps.put("total", rowCount) ;
		maps.put("rows", infos) ;
		return maps;
	}

	private int getRowCount() {
		int rowCount = 0 ;
		rs = commonQuery("select count(*) from list_request", null) ;
		try {
			if(rs.next()) {
				rowCount = rs.getInt(1) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return rowCount;
	}

	private List<InfoRequest> getAll(int page, int rows, String sort,
			String order) {
		List<InfoRequest> infos = new ArrayList<InfoRequest>() ;
		String sql = "select * from list_request where 1=1 " ;
		if(sort != null && order != null) {
			sql += " order by "+sort +" " + order ; 
		}
		sql += " limit ?,?" ;
		rs = commonQuery(sql, rows*(page-1),rows) ;
		try {
			while(rs.next()) {
				InfoRequest info = new InfoRequest() ;
				info.setId(rs.getInt("id")) ;
				info.setDate(rs.getString("date")) ;
				info.setName(rs.getString("name")) ;
				info.setNumber(rs.getString("number")) ;
				info.setNews(rs.getString("news")) ;
				info.setListType(rs.getString("listType"));
				infos.add(info) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return infos;
	}

	@Override
	public void delete(int id) {
		
		commonUpdate("delete from list_request where id =?", id) ;
		
	}

	@Override
	public int into(String name,String number, String address, String birthday,String type) {
			int result=0;
			String sql ="INSERT INTO list_request(name,number,date,news,listType) values(?,?,?,?,?)" ;
			result=commonUpdate(sql, name,number,address,birthday) ;
			return result;
		}

		
		@Override
		public int edit( String name, String number, String address,String birthday,String type,int id) {
			
			String sql ="UPDATE list_request SET name=?,number=?,date=?,news=?,listType=? WHERE id=?" ;
			int result=commonUpdate(sql, name,number,address,birthday,type,id) ;
			return result;
		}

		@Override
		public Map<String, Object> sel(int page, int rows, String sort,
				String order, String name) {
			Map<String,Object> maps = new HashMap<String, Object>();
			List<InfoRequest> infos = select(page,rows,sort,order,name);
			int rowCount = getRowCount() ;
			maps.put("total", rowCount) ;
			maps.put("rows", infos) ;
			return maps;
		}
		private List<InfoRequest> select(int page, int rows, String sort,
				String order,String name) {
			List<InfoRequest> infos = new ArrayList<InfoRequest>() ;
			String sql = "select * from list_request where name=? and 1=1 " ;
			if(sort != null && order != null) {
				sql += " order by "+sort +" " + order ; 
			}
			sql += " limit ?,?" ;
			rs = commonQuery(sql, rows*(page-1),rows) ;
			try {
				while(rs.next()) {
					InfoRequest info = new InfoRequest() ;
					info.setId(rs.getInt("id")) ;
					info.setName(rs.getString("name")) ;
					info.setNumber(rs.getString("number")) ;
					info.setDate(rs.getString("date")) ;
					info.setNews(rs.getString("news")) ;
					info.setListType(rs.getString("listType"));
					infos.add(info) ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				closeAll(rs, pstmt, conn) ;
			}
			return infos;
		}
		
}
