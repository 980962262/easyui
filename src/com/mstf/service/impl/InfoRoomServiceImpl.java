package com.mstf.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mstf.bean.InfoRoom;
import com.mstf.service.InfoRoomService;
import com.mstf.util.DBUtil;

public class InfoRoomServiceImpl extends DBUtil implements InfoRoomService {
	//宿舍信息表
	@Override
	public Map<String, Object> findAll(int page, int rows, String sort,
			String order) {
		Map<String,Object> maps = new HashMap<String, Object>();
		List<InfoRoom> infos = getAll(page,rows,sort,order);
		int rowCount = getRowCount() ;
		maps.put("total", rowCount) ;
		maps.put("rows", infos) ;
		return maps;
	}

	private int getRowCount() {
		int rowCount = 0 ;
		rs = commonQuery("select count(*) from room", null) ;
		try {
			if(rs.next()) {
				rowCount = rs.getInt(1) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return rowCount;
	}

	private List<InfoRoom> getAll(int page, int rows, String sort,
			String order) {
		List<InfoRoom> infos = new ArrayList<InfoRoom>() ;
		String sql = "select * from room where 1=1 " ;
		if(sort != null && order != null) {
			sql += " order by "+sort +" " + order ; 
		}
		sql += " limit ?,?" ;
		rs = commonQuery(sql, rows*(page-1),rows) ;
		try {
			while(rs.next()) {
				InfoRoom info = new InfoRoom() ;
				info.setId(rs.getInt("id")) ;
				info.setName(rs.getString("roomName")) ;
				info.setType(rs.getString("roomType")) ;
				info.setNews(rs.getString("roomNews")) ;
				infos.add(info) ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeAll(rs, pstmt, conn) ;
		}
		return infos;
	}

	@Override
	public void delete(int id) {
		commonUpdate("delete from room where id =?", id) ;
	}

	@Override
	public int into(String name, String type, String news) {
			int result=0;
			System.out.println(name+type+news);
			String sql ="INSERT INTO room(roomName,roomType,roomNews) values(?,?,?)" ;
			result=commonUpdate(sql, name,type,news) ;
			return result;
		}

		
		@Override
		public int edit( String name, String type, String news,int id) {
			
			String sql ="UPDATE room SET roomName=?,roomType=?,roomNews=? WHERE id=?" ;
			int result=commonUpdate(sql, name,type,news,id) ;
			return result;
		}

		@Override
		public Map<String, Object> sel(int page, int rows, String sort,
				String order, String name) {
			Map<String,Object> maps = new HashMap<String, Object>();
			List<InfoRoom> infos = select(page,rows,sort,order,name);
			int rowCount = getRowCount() ;
			maps.put("total", rowCount) ;
			maps.put("rows", infos) ;
			return maps;
		}
		private List<InfoRoom> select(int page, int rows, String sort,
				String order,String name) {
			List<InfoRoom> infos = new ArrayList<InfoRoom>() ;
			String sql = "select * from room where roomName=? and 1=1 " ;
			if(sort != null && order != null) {
				sql += " order by "+sort +" " + order ; 
			}
			sql += " limit ?,?" ;
			rs = commonQuery(sql, rows*(page-1),rows) ;
			try {
				while(rs.next()) {
					InfoRoom info = new InfoRoom() ;
					info.setId(rs.getInt("id")) ;
					info.setName(rs.getString("roomName")) ;
					info.setType(rs.getString("roomType")) ;
					info.setNews(rs.getString("roomNews")) ;
					infos.add(info) ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				closeAll(rs, pstmt, conn) ;
			}
			return infos;
		}
		
}
