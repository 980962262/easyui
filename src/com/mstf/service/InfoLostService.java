package com.mstf.service;

import java.util.Map;

public interface InfoLostService {

	Map<String, Object> findAll(int page, int rows, String sort,
			String order);

	void delete(int parseInt);

	int into(String name, String number, String address, String birthday);

	int edit(String name, String number, String address, String birthday, int id);

	Map<String, Object> sel(int page, int rows, String sort, String order,
			String name);

}
