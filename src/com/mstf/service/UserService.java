package com.mstf.service;

import com.mstf.bean.Sudents;
import com.mstf.bean.User;

public interface UserService {

	User getUserByNameAndPwd(String uname, String upass);
	
	Sudents getUserStuByNameAndPwd(String uname, String upass);

	int save(String name,String pass);

}
