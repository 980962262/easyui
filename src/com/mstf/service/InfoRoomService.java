package com.mstf.service;

import java.util.Map;

public interface InfoRoomService {

	Map<String, Object> findAll(int page, int rows, String sort,
			String order);

	void delete(int parseInt);

	int into(String name, String type, String news);

	int edit(String name, String type, String news, int id);

	Map<String, Object> sel(int page, int rows, String sort, String order,
			String name);

}
