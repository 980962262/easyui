package com.mstf.service;

import java.util.Map;

public interface InfoProblemService {

	Map<String, Object> findAll(int page, int rows, String sort,
			String order);

	void delete(int parseInt);

	int into(String date, String problem);

	int edit(String date, String problem, int id);

	Map<String, Object> sel(int page, int rows, String sort, String order,
			String name);

}
