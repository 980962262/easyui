package com.mstf.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 数据库连接的工具类
 * @author xrd 最代码官方验证通过
 *
 */
public class DBUtil {
	private static final String DRIVER_CLASS="com.mysql.cj.jdbc.Driver" ;
	private static final String DRIVER_URL="jdbc:mysql://localhost:3306/home?useSSL=false&serverTimezone=UTC" ;
	private static final String DRIVER_USER="root" ;
	private static final String DRIVER_PWD="123456" ;
	
	protected Connection conn ;
	
	protected PreparedStatement pstmt ;
	
	protected ResultSet rs ;
	
	static {
		try {
			Class.forName(DRIVER_CLASS) ;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		System.out.println(getConnection()) ;
	}
	/**
	 * 通用的数据库连接方法
	 * @return
	 */
	public static Connection getConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DRIVER_URL, DRIVER_USER, DRIVER_PWD) ;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn ;
	}
	/**
	 * 通用的查询方法
	 * @param sql
	 * @param params
	 * @return
	 */
	public ResultSet commonQuery(String sql,Object...params) {
		conn = getConnection() ;
		
		try {
			pstmt = conn.prepareStatement(sql) ;
			
			if(params != null && params.length > 0 ) {
				setValues(pstmt,params);
			}
			
			rs = pstmt.executeQuery() ;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs ;
	}
	/**
	 * 通用的增，删，改方法
	 * @param sql sql语句
	 * @param params 代表sql语句中的参数
	 * @return  insert into students(name,addree) values(?,?)
	 */
	public int commonUpdate(String sql,Object...params) {
		int result = 0 ;
		conn = getConnection() ;
		try {
			pstmt = conn.prepareStatement(sql) ;
			if(params != null && params.length > 0 ) {
				setValues(pstmt,params);
			}
			result = pstmt.executeUpdate() ;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll(rs,pstmt,conn);
		}
		return result ;
	}
	/**
	 * 通用的数据库关闭方法
	 */
	protected void closeAll(ResultSet rs, PreparedStatement pstmt,
			Connection conn) {
		try {
			if(rs != null) {
				rs.close() ;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(pstmt != null) {
					pstmt.close() ;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(conn != null) {
					try {
						conn.close() ;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	/**
	 * 这个方法主要是用来，设置sql语句的参数
	 * @param pstmt2
	 * @param params
	 * 
	 *   commonUpdate(insert into students(name,addree) values(?,?),"ss","yy")
	 * @throws SQLException 
	 */
	private void setValues(PreparedStatement pstmt2, Object...params) throws SQLException {
		for(int i = 0 ;i<params.length ;i++) {
			
			pstmt2.setObject(i+1, params[i])  ;
		}
		
	}
}
