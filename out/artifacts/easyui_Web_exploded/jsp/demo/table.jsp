<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<jsp:include page="../../inc/meta.jsp" />
	<jsp:include page="../../inc/esyui.jsp" />
	<script type="text/javascript">
		$(function(){
			$('#dg').datagrid({   
			    url:sy.bp()+'/InfoAction',   
			    fitColumns:true,
			    nowrap:false,
			    idField:'id',
			    pagination:true,
			    pageSize:2,
			    pageList:[2,4,6,8],
			    sortName:'id',
			    sortOrder:'desc',
			    columns:[[   
			        {field:'id',sortable:true,title:'productid',width:100},   
			        {field:'name',title:'productname'},   
			        {field:'age',title:'unitcost',width:100},   
			        {field:'address',title:'status',width:100},   
			        {field:'birthday',title:'listprice',width:100}
			    ]]
			      
			});  
			
		});
	</script>
  </head>
  
 <body>  
 	<table id="dg"></table> 
</body>  

</html>
