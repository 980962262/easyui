<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<jsp:include page="../../inc/meta.jsp" />
	<jsp:include page="../../inc/esyui.jsp" />
  </head>
  
 <body class="easyui-layout">  
    <div data-options="region:'north',title:'North Title',split:true,href:'${pageContext.request.contextPath}/HomeAction?action=north'" style="height:100px;"></div>  
    <div data-options="region:'south',title:'South Title',split:true,href:'${pageContext.request.contextPath}/HomeAction?action=south'" style="height:100px;"></div>  
    <div data-options="region:'east',iconCls:'icon-reload',title:'East',split:true,href:'${pageContext.request.contextPath}/HomeAction?action=east'" style="width:100px;"></div>  
    <div data-options="region:'west',title:'West',split:true,href:'${pageContext.request.contextPath}/HomeAction?action=west'" style="width:180px;"></div>  
    <div data-options="region:'center',title:'center title',href:'${pageContext.request.contextPath}/HomeAction?action=center'" style="padding:5px;background:#eee;">
    </div>  
</body>  

</html>
