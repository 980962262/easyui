<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>My JSP 'loginandreg.jsp' starting page</title>
    <jsp:include page="../../inc/meta.jsp" />
    <jsp:include page="../../inc/esyui.jsp" />
    <script type="text/javascript" charset="UTF-8">
       sy.ns("sy.user");
       $(function(){
       		/**这是渲染登录的*/
       		sy.user.loginDialog = $('#loginDialog').dialog({
       			title: '用户登录',   
			    width: 300,   
			    height: 260,   
			    closed: false,   
			    cache: false,   
			    modal: true,
			    draggable:true,
       			closable:true,
       			toolbar:[{
       				text:'帮助',
       				iconCls:'icon-help',
       				handler:function() {
       					alert('这是一个宿舍管理系统');
       				}
       			}],
       			buttons:[{
       				text:'登录',
       				iconCls:'icon-add',
       				handler:function() {
       				   /**这是采用AJAX提交方式
       				   if($('#loginForm').form('validate'))
       					login();*/
       					// form的第一种方式
       					//sy.user.loginForm.submit() ;
       					$('#loginForm').form('submit', {  
       						url: sy.bp()+"/UserAction?action=login",
						    success: function(data){   
						        var data = eval('(' + data + ')');  // change the JSON string to javascript object   
						        if(data && data.flag) {
						        	sy.user.loginDialog.dialog('close');
							      	$.messager.show({
										title:'提示信息',
										msg:'当前的用户【'+data.obj.name+'】,'+data.msg,
										timeout:5000,
										showType:'slide'
									});
							      } else {
							      	$.messager.alert('提示信息',data.msg);  
							   }  
						    }   
						});  
       				}
       			},{
       				text:'注册',
       				iconCls:'icon-redo',
       				handler:function(){
       					// 1.关闭当前的，然后打开注册页面
       					sy.user.loginDialog.dialog('close');
       					sy.user.regDialog.dialog('open') ;
       				}
       			}]
       		}) ;
       		/**这是渲染登录的结束*/
       		/*这是渲染注册*/
       		sy.user.regDialog = $('#regDialog').show().dialog({
       			title: '用户注册',
			    width: 300,   
			    height: 260,   
			    closed: true,   
			    cache: false,   
			    modal: true,
			    draggable:true,
       			closable:true,
       			buttons:[{
       				text:'注册',
       				iconCls:'icon-redo',
       				handler:function(){
       					reg();
       				}
       			}]
       		}) ;
       		/*这是渲染注册结束*/
       		/*使用easyUI的方式来进行渲染登录form的第一种方式*/
       		sy.user.loginForm = $('#loginForm').form({
			    url:sy.bp()+"/UserAction?action=login",   
			    success:function(data){ 
			    // change the JSON string to javascript object 
			    // {"obj":{"name":"sa","id":1,"pass":"sa"},"msg":"登录成功","flag":true}   
			      //var data = eval('(' + data + ')'); 
			      var data = $.parseJSON(data) ;
			      if(data && data.flag) {
			      	$.messager.show({
						title:'提示信息',
						msg:'当前的用户【'+data.obj.name+'】,'+data.msg,
						timeout:5000,
						showType:'slide'
					});
			      } else {
			      	$.messager.alert('提示信息',data.msg);  
			      }  
			    }   
			});
       });	
       /**这是ajax的提交方式*/
       /*编写一个登录函数*/
       function login() {
       		$.ajax({
			   type: "POST",
			   url: sy.bp()+"/UserAction?action=login",
			   data: $('#loginForm').serialize(),
			   dataType:"json" ,
			   //var daa =  {"obj":{"name":"sa","id":1,"pass":"sa"},"msg":"登录成功","flag":true}
			   success: function(data){
			      if(data && data.flag) {
			      	
			      	$.messager.show({
						title:'提示信息',
						msg:'当前的用户【'+data.obj.name+'】,'+data.msg,
						timeout:5000,
						showType:'slide'
					});
			      } else {
			      	$.messager.alert('提示信息',data.msg);  
			      }
			   }
			});
       }
       /*编写一个登录函数结束*/
       /*编写一个注册函数*/
       function reg() {
       		$.ajax({
			   type: "POST",
			   url: sy.bp()+"/UserAction?action=reg",
			   data: $('#regForm').serialize(),
			   dataType:"json" ,
			   //var daa =  {"obj":{"name":"sa","id":1,"pass":"sa"},"msg":"登录成功","flag":true}
			   success: function(data){
			      if(data && data.flag) {
			      	$.messager.show({
						title:'提示信息',
						msg:data.msg,
						timeout:5000,
						showType:'slide'
					});
			      } else {
			      	$.messager.alert('提示信息',data.msg);  
			      	
			      }
			   }
			});
       }
       /*编写一个注册函数结束*/
       /*ajax提交方式结束*/

       //退出
       function logout() {
           $.ajax({
               type: "POST",
               url: sy.bp()+"/UserAction?action=logout",
               data: $('#regForm').serialize(),
               dataType:"json" ,
               success: function(data){
                   if(data && data.flag) {
                       $.messager.show({
                           title:'提示信息',
                           msg:data.msg,
                           timeout:5000,
                           showType:'slide'
                       });
                       sy.user.loginDialog.dialog('open');
                   } else {
                       $.messager.alert('提示信息',data.msg);

                   }
               }
           });
       }
    </script>
  </head>
  
  <body style="background-color:#e4f7ff;">
    <div id="loginDialog">
    	<form id="loginForm" class="easyui-form" name="form1" method="post" style="text-align:center;margin-top:20px;">
    		
    		用户名：<input type="text" class="easyui-validatebox" data-options="required:true" name="uname" style="margin-bottom:20px;width:160px;"/><br/>
    		
    		密&nbsp;&nbsp;&nbsp;码：<input type="password" name="upass" style="width:160px;"/><br/>
    		
    		身份：<select name="sf">
    			<option selected="selected" value="管理员" >管理员</option>
    			<option value="学生">学生</option>
    		</select><br/>
    	</form>
   	   
    
    </div>  
    
    
    <div id="regDialog" style="display:none;">
    	<form id="regForm" name="form2" method="post" style="text-align:center;margin-top:20px;">
    		name:<input type="text" name="name" style="margin-bottom:20px;width:160px;"/><br/>
    		
    		pass:<input type="text" name="pass" style="margin-bottom:20px;width:160px;"/><br/>
    		
    		
    	</form>
   	   
    
    </div>
 
  </body>
</html>
