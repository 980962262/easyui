<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript">
		$(function(){
			$('#centerTabs').tabs({
				border:false,
				title:'首页',
				fit:true,
				closeable:true
			});

			setTimeout(function(){
			    var href=sy.bp()+"/HomeAction?action=home" ;
				$('#centerTabs').tabs('add',{
				    title:'首页',
				    content:'<iframe src="'+href+'" frameborder="0px" scrolling="no" style="width:100%;height:99.8%;border:0px;"></iframe>',
				    closable:true,
				    fit:true
				});
			}, 0);

		});

		function addTabFun(ops) {
			var href= sy.bp()+"/"+ops.src ;
			if($('#centerTabs').tabs('exists',ops.title)) {
				$('#centerTabs').tabs('close',ops.title)
			}
			$('#centerTabs').tabs('add',{
			    title:ops.title,
			    content:'<iframe src="'+href+'" frameborder="0px" scrolling="no" style="width:100%;height:99.8%;border:0px;"></iframe>',
			    closable:true,
			    fit:true
			});
		}
	</script>

    <div id="centerTabs">
    	欢迎使用校园宿舍管理系统
    </div>
