<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<jsp:include page="inc/meta.jsp" />
	<jsp:include page="inc/esyui.jsp" />
  <script type="text/javascript" src="js/jquery.portal.js"></script>
 	<script>
 	    var divLength ;
		$(function(){
			$('#pp').portal({
				border:false,
				fit:true
			});
			divLength = $('#pp div').length ;
			add();
		});
		function add(){
		
		   $.ajax({
		   	   type: "POST",
			   url: "PortalAction",
			   dataType:"json",
			   success: function(data){
			     if(data) {
			     	for(var i = 0 ;i<data.length ;i++) {
			     		var href =sy.bp()+'/portal/'+data[i].path;
			     		console.info(href+"***********");
			     		var p = $('<div/>').appendTo('body');
						p.panel({
							title:data[i].title,
							content:'<iframe src="'+href+'" scrolling="no" frameborder="0px" style="width:100%;height:99.8%;border:0px"></iframe>',
							height:100,
							closable:true,
							collapsible:true
						});
						$('#pp').portal('add', {
							panel:p,
							columnIndex:i%divLength

						});
					 }
					  $('#pp').portal('resize');
			     	}
			     
			     }
			   });
		}
		function remove(){
			$('#pp').portal('remove',$('#pgrid'));
			$('#pp').portal('resize');
		}
	</script>
  </head>
  
 <body class="easyui-layout">
	<div region="center" border="false">
		<div id="pp" style="position:relative">
			<div style="width:30%;">
				
			  
			</div>
			<div style="width:40%;">
				
			</div>
			<div style="width:30%;">
				
			</div>
		</div>
	</div>
</body>  

</html>
